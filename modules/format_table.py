#! /usr/bin/python
## @file
#  Extract information from GenBank files.
import re
import extract_genbank
from collections import defaultdict

## Matches Genotype annotation. Either Sample.GT or just GT.
pattern_GT = re.compile('((.*)\.){0,1}GT$')
## Matches Genotype. For example A/G or AAG/AC 
pattern_GT_comps = re.compile('(\w*)/(\w*)')

## Write header for variant table.
#  @param infile Variant table file.
#  @param samples List of sample names.
def write_info(infile, samples):
    infile.write("# CHROM: contig\n")
    infile.write("# POS  : coordinates\n")
    infile.write("# REF  : reference allele\n")
    infile.write("# ALT  : alternative allele\n")
    infile.write("# AAR  : reference amino acid\n")
    infile.write("# AAA  : alternative amino acid\n")
    infile.write("# QUAL : PHRED-scaled probability that REF/ALT polymorphism exists\n")
    infile.write("# TYPE : type of feature (Sequence Ontology term)\n")
    infile.write("# AAN  : amino acid number\n")
    infile.write("# NTN  : nucleotide number\n")
    infile.write("# ACC  : accession of affected protein\n")
    infile.write("# GENE : name of affected gene\n")
    infile.write("# DESC : description of affected protein\n")
    infile.write("# NCBI : link to NCBI entry\n")
    infile.write("# UNIP : link to UNIPROT entry\n")
    infile.write("# GT   : Genotype\n")
    infile.write("# GQ   : PHRED-scaled probability that genotype is correct\n")
    infile.write("# COV  : coverage\n")
    infile.write("# COVR : number of reads that support REF\n")
    infile.write("# COVA : number of reads that support ALT\n")
    if samples[0] is not None:
        infile.write("# -> Sample Identifiers <-\n")
        for i, sample in enumerate(samples):
            infile.write("# S{}: {}\n".format('{}{}'.format(i+1, ' '*max(1, (4-len(str(i))))), sample))

## Extract samples from the header of a Variant table file.
#  @param infile Variant table file.
def get_samples(infile):
    with open(infile, 'r') as f:
        header = next(f)
    header = header.strip().split('\t')
    header = header[6:]
    sample_length = len(header)
    samples = list()
    #if sample_length > 4: # if multisample
    global pattern_GT
    while sample_length > 0:
        match = re.match(pattern_GT, header[0]).group(2)
        if match is None:
            samples.append('UNIDENTIFIED')
        else:
            samples.append(match)
        sample_length -= 4
        header = header[4:]
    #else:
    #    samples.append(None)
    return samples

## Transform variant table file to a custom format including annotations.
#  @param infile Input variant table file.
#  @param outfile Output formatted variant table file.
#  @param anno_dict Dictionary of annotations.
#  @param dec Decimal separator.
def format_table(infile, outfile, anno_dict, dec='.'):
    samples = get_samples(infile)
    row = dict()
    row['GT'] = dict()
    row['GQ'] = dict()
    row['COV'] = dict()
    row['AD'] = dict()
    row['COVR'] = dict()
    row['COVA'] = dict()
    with open(infile, 'r') as f, open(outfile, 'w') as g:
        next(f)
        write_info(g, samples)
        fixed_header = ['CHROM', 'POS', 'REF', 'ALT', 'AAR', 'AAA', 'QUAL', 'TYPE', 'AAN', 'NTN', 'ACC', 'GENE', 'DESC', 'NCBI', 'UNIP']
        if samples[0] is None:
            variable_header = ['GT', 'GQ', 'COV', 'COVR', 'COVA']
        else:
            variable_header = list()
            for i in range(len(samples)):
                variable_header += ['S{}.GT'.format(i+1), 'S{}.GQ'.format(i+1), 'S{}.COV'.format(i+1), 'S{}.COVR'.format(i+1), 'S{}.COVA'.format(i+1)]
        header_comb = fixed_header + variable_header
        header_row = ('{}\t'*(len(header_comb)-1)+'{}\n').format(*header_comb)
        g.write(header_row)
        for line in f:
            values = line.strip().split('\t')
            row['CHROM'] = values[0]
            row['POS'] = values[1]
            row['REF'] = values[2]
            row['ALT'] = values[3]
            row['QUAL'] = str(values[4]).replace('.', dec)
            row['ANN'] = values[5]
            for i, sample in enumerate(samples): 
                row['GT'][sample] = values[6+i*4]
                try:
                    m = re.match(pattern_GT_comps, row['GT'][sample])
                    if m.group(1) == m.group(2):
                        row['GT'][sample] = m.group(1)
                except:
                    pass
                row['GQ'][sample] = values[7+i*4]
                row['COV'][sample] = values[8+i*4]
                row['AD'][sample] = values[9+i*4]
                # split REF,ALT coverages
                cov = row['AD'][sample].split(',')
                if cov == ['NA']:
                    row['COVA'][sample] = 'NA'
                    row['COVR'][sample] = 'NA'
                else:
                    row['COVA'][sample] = cov[1]
                    row['COVR'][sample] = cov[0]
            ann_processed = process_ann(row['ANN'])
            #anno = anno_dict[ann_processed['locus_tag']]
            try:
                anno = anno_dict[ann_processed['gene_name']]
                row['GENE'] = anno[0]
                row['ACC'] = anno[1]
                row['DESC'] = anno[2]
                if len(row['ACC']) != 0:
                    row['NCBI'] = 'http://www.ncbi.nlm.nih.gov/nuccore/' + row['ACC']
                else:
                    row['NCBI'] = ''
                if len(anno[3]) != 0:
                    row['UNIPROT'] = 'http://www.uniprot.org/uniprot/' + anno[3]
                else:
                    row['UNIPROT'] = ''
            except: #no entry
                row['GENE'] = ''
                row['ACC'] = ''
                row['DESC'] = ''
                row['NCBI'] = ''
                row['UNIPROT'] = ''
            output_fixed = [row['CHROM'], row['POS'], row['REF'], row['ALT'], ann_processed['AAR'], ann_processed['AAA'], row['QUAL'], ann_processed['TYPE'], ann_processed['AAN'], ann_processed['NTN'], row['ACC'], row['GENE'], row['DESC'], row['NCBI'], row['UNIPROT']]
            output_variable = list()
            for sample in samples:
                output_variable += [row['GT'][sample], row['GQ'][sample], row['COV'][sample], row['COVR'][sample], row['COVA'][sample]]
            output_comb = output_fixed + output_variable
            g.write(('{}\t'*(len(output_comb)-1)+'{}\n').format(*output_comb))

## Tranform annotation to a dictionary.
#  @param annotation Annotation.
def process_ann(annotation):
    ann_dict = dict()
    ann = annotation.split('|')
    ann_dict['allele'] = ann[0]
    ann_dict['annotation'] = ann[1]
    ann_dict['annotation_impact'] = ann[2]
    ann_dict['gene_name'] = ann[3]
    ann_dict['gene_id'] = ann[4]
    ann_dict['feature_type'] = ann[5]
    ann_dict['feature_id'] = ann[6]
    ann_dict['transcript_biotype'] = ann[7]
    ann_dict['rank'] = ann[8]
    ann_dict['hgvs.c'] = ann[9]
    ann_dict['hgvs.p'] = ann[10]
    ann_dict['cdnaposlength'] = ann[11]
    ann_dict['cdsposlength'] = ann[12]
    ann_dict['aaposlength'] = ann[13]
    ann_dict['distance'] = ann[14]
    ann_dict['errors'] = ann[15]
    processed = dict()
    try:
        aa_change = re.match('p\.(\D+)(\d+)(\D+)', ann_dict['hgvs.p']).groups()
        processed['AAR'] = aa_change[0]
        processed['AAA'] = aa_change[2]
        processed['AAN'] = aa_change[1]
        nt_change = re.match('c\.(\d+)[>\D]+', ann_dict['hgvs.c']).groups()
        processed['NTN'] = nt_change[0]
    except AttributeError:
        processed['AAR'] = ''
        processed['AAA'] = ''
        processed['AAN'] = ''
        processed['NTN'] = ''
    processed['TYPE'] = ann_dict['annotation']
    #processed['locus_tag'] = ann_dict['feature_id']
    processed['gene_name'] = ann_dict['gene_name']
    return processed 
